//
// main.cpp
// Exercise 3
// Created by Tulasi Ram Reddy Badduri on 01/30/2017
// 
#include<iostream>
int main()
{
	int i;
	for (i = 1; i <= 100; i++)
	{
		if (i % 15 == 0)
			std::cout << "FIZZ BUZZ" << std::endl;
		else if (i % 3 == 0)
			std::cout << "FIZZ" << std::endl;
		else if(i % 5 == 0)
			std::cout << "BUZZ" << std::endl;
		else
			std::cout << i << std::endl;
	}
	getchar();
		return 0;
}